require 'httparty'
require 'fileutils'
require 'os'

new_file_xsweet = File.join(Dir.pwd, "xsweet.zip")
new_file_typescript = File.join(Dir.pwd, "typescript.zip")
new_file_htmlevator = File.join(Dir.pwd, "htmlevator.zip")

downloaded_file = File.new(new_file_xsweet, "w")
downloaded_file.write(HTTParty.get("https://gitlab.coko.foundation/XSweet/XSweet/repository/archive.zip?ref=master").body)
downloaded_file.close

downloaded_file = File.new(new_file_typescript, "w")
downloaded_file.write(HTTParty.get("https://gitlab.coko.foundation/XSweet/editoria_typescript/repository/archive.zip?ref=master").body)
downloaded_file.close

downloaded_file = File.new(new_file_htmlevator, "w")
downloaded_file.write(HTTParty.get("https://gitlab.coko.foundation/XSweet/HTMLevator/repository/archive.zip?ref=master").body)
downloaded_file.close

if OS.mac?
# on linux change open to xdg-open
%x`open #{new_file_xsweet}`
sleep(1)
%x`open #{new_file_typescript}`
sleep(1)
%x`open #{new_file_htmlevator}`
sleep(1)
else
%x`xdg-open #{new_file_xsweet}`
sleep(1)
%x`xdg-open #{new_file_typescript}`
sleep(1)
%x`xdg-open #{new_file_htmlevator}`
sleep(1)
end

typescript_name = Dir.glob("editoria_typescript*").pop
xsweet_name = Dir.glob("XSweet-*").pop
htmlevator_name = Dir.glob("HTMLevator-*").pop

puts "typescript_name"
puts typescript_name
puts "xsweet_name"
puts xsweet_name
puts "htmlevator_name"
puts htmlevator_name

%x`mv #{typescript_name} #{xsweet_name}/applications/typescript`
%x`mv #{htmlevator_name} #{xsweet_name}/applications/htmlevator`
%x`mkdir #{xsweet_name}/scripts`

%x`cp ./execute_chain.sh #{xsweet_name}/scripts`

FileUtils.rm "xsweet.zip"
FileUtils.rm "typescript.zip"
FileUtils.rm "htmlevator.zip"