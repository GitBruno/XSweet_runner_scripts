#!/usr/bin/env ruby

puts "Unzipping docs..."

require 'fileutils'

# dir = '/Users/atheg/Desktop/crawler/to_convert'
convertDir = Dir.getwd + "/to_convert"

book_list = Dir["#{convertDir}/*"].select {|cand| File.directory? cand}

book_list.each do |book|

  docx_list = Dir["#{book}/*.docx"]

  docx_list.each do |docx|
    no_space =  docx.split(" ").join
    if docx != no_space
      FileUtils.mv(docx, no_space)
    end

    zip_name_no_ext = no_space.slice(0...-5)
    zip_name_ext = zip_name_no_ext + ".zip"

    FileUtils.cp(no_space, zip_name_ext)

    # %x`open #{zip_name_ext}`

    %x`unzip -o #{zip_name_ext} -d #{zip_name_no_ext}`

  end

  sleep(8)

  zip_list = Dir["#{book}/*.zip"]
  zip_list.each do |zip|
    FileUtils.rm zip
  end

end

puts "Converting docs..."

xsweet_script_path = Dir.glob("XSweet-*").pop

rootDir = Dir.getwd
convertDir = rootDir + "/to_convert"

book_list = Dir["#{convertDir}/*"].select {|cand| File.directory? cand}
book_list.delete("#{convertDir}/temp")

puts "BOOK LIST"
number_label = 1
book_list.each do |book|
  puts "#{number_label}: #{book}"
  number_label += 1
end
puts "BOOK LIST END"

book_list.each do |book|
  book = book.split("/").last
  puts "BOOK: #{book}"

  file_path_list = Dir["#{convertDir}/#{book}/*"].select {|cand| File.directory? cand}
  # puts file_path_list

  file_list = []
  file_path_list.each do |file_path|
    file_list << file_path.split("/").last
  end

  newDir = rootDir + "/#{xsweet_script_path}/scripts"

  Dir.chdir newDir

  file_list.each do |chapter|
    puts "converting: #{chapter}"
    %x`sh execute_chain.sh #{convertDir}/#{book} #{book} #{chapter}`
  end

  puts "done with #{book}"
end
