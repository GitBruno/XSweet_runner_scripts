# XSweet scripts

These ruby and bash scripts are designed to allow you to run the XSweet suite of XSLTs locally, with a copy of every step's output and input.

## Requirements

These scripts assume you have an Unix-like environment and Ruby installed (no specific version requirements have been tested)
You will also need to make sure you have the following gems:
`gem install file-utils httparty os`

## 1. Clone the repo

Clone this repo and `cd` into it:

`git clone https://gitlab.coko.foundation/XSweet/XSweet_runner_scripts`

`cd XSweet_runner_scripts`

## 2. Download the latest XSweet `master` branches

Run:

`ruby xsweet_downloader.rb`

This downloads the `master` branches of the 3 XSweet repositories as .zip files:

* XSweet Core: https://gitlab.coko.foundation/XSweet/XSweet/
* HTMLevator: https://gitlab.coko.foundation/XSweet/HTMLevator/
* Editoria Typescript: https://gitlab.coko.foundation/XSweet/editoria_typescript/

These are then unzipped, and `HTMLevator` and `Editoria Typescript` are moved into the XSweet Core directory (e.g. `XSweet-master-b58cce5612ef70d5da4e5dbd6cabd1babd510eca`). This script also copies the `execute_chain.sh` file into the XSweet Core directory.

## 3. Add folders containing `.docx` files to the `to_convert` folder

For these scripts to work properly, `.docx` files _must_ be placed inside an additional folder in the `to_convert` folder. These scripts were meant to convert many books at a time, so they look for _directories_ inside the `to_convert` directory that contain one or more `.docx` files:

For example, these scripts will find and convert this .docx:

`to_convert/alex/alexs_book_file.docx`

But if the `.docx` is placed directly in the `to_convert` directory, without being nested in another directory inside `to_convert`, it won't be converted. This will _not_ be converted:

`to_convert/alexs_book_file.docx`

## 4. Create an unzipped copy of the `.docx` files

MS Word `.docx` files are really compressed archives. By changing a `.docx` extension to `.zip`, you can unzip these archives. These scripts rely on the raw, unzipped XML files to work.

If this is the first time you are converting a given `.docx` file, you will need to run the `docx_unzipper.rb` script.

`ruby docx_unzipper.rb`

This script:
* indexes all `.docx` files in the `to_convert` directory
* removes spaces from the `.docx` file names
* copies each `.docx` and changes the copy's file extension from `.docx` to `.zip`
* unzips each archive, then deletes the `.zip` file

After the script has run, you'll have an unzipped copy of every `.docx` file alongside the original file. Leave both the original and unzipped files in place: the next script searches for `.docx` files to convert, but pulls the underlying data for conversion from the corresponding unzipped directory.

You only need to run this script once each time you add new `.docx` files to convert - once you've got a `.docx` and its unzipped counterpart, you can run the conversions any number of times.

## 5. Run the conversions

The command:

`ruby xsweet_runner.rb`

runs the conversion chain (as specified in the `execute_chain.sh` file) on every `.docx` file in the `to_convert` directory. The converted files are output to an `output` directory inside the XSweet Core directory.
* The first step specified by the `execute_chain.sh` script uses the `.docx` file's `document.xml` sheet (and other data files from the Word file) as its input.
* With a few exceptions, each step outputs a new file, and the next step uses the previous output as its input.

> You can run step 4 and 5 in one hit with [`xsweet_unzip-n-run.rb`](xsweet_unzip-n-run.rb) please note that this script will overwrite existing files WITHOUT prompting.

## Important notes

These scripts were built for rapid development and testing of XSweet, and as such, there are some important gotchas to be aware of:
* All scripts _must_ be run from the root directory of this repo.
* You can reconfigure which XSL sheets are run and in what order by editing the `execute_chain.sh` script. But note that when the `xsweet_downloader.rb` script is run, in addition to downloading the latest XSweet repos, it also copies the `execute_chain.sh` script in this repo's root directory into a `scripts` directory in the XSweet core directory. It is this copy that is invoked by the `xsweet_runner.rb`. Thus, modifying the `xsweet_runner.rb` file in the `XSweet_scripts` root directory has no effect on the conversion until you either copy it into the XSweet Core directory's `scripts` folder, or rerun the `xsweet_downloader` script.
* `xsweet_runner.rb` determines the XSweet Core repository to use by searching in this repo's root directory for a directory name that starts with "XSweet..." (e.g. "XSweet-master-b58cce5612ef70d5da4e5dbd6cabd1babd510eca"). If you want to download a new version of XSweet but keep the old copy you were working off, either move it to a different directory or append something to the beginning of the file name.
* Finally, these scripts are not maintained, supported, etc. - they are merely handy development tools to use as-is.
