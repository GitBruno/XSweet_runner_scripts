#!/bin/bash
# For producing HTML5 outputs via XSweet XSLT from sources extracted from .docx (Office Open XML)

# $DOCNAME is any short identifier
P=$1
BOOKNAME=$2
DOCNAME=$3
# $DOCXdocumentXML is the 'word/document.xml' file extracted (unzipped) from a .docx file
# (Also, its neighbor files from the .docx package should be available.)
# DOCXdocumentXML=$2

# Bind $DOCXdocumentXML and $DOCNAME via $1 and $2 and try
# CL > ./ExtractandRefine.sh yourdocumentname path/to/your/document.xml
# (Which would make it possible to call this script from another one
# and even loop over file sets.)

# Note Saxon is included with this distribution, qv for license.
saxonHE="java -jar ../lib/SaxonHE9-8-0-1J/saxon9he.jar"  # SaxonHE (XSLT 2.0 processor)

# EXTRACTION
EXTRACT="../applications/docx-extract/docx-html-extract.xsl"                       # "Extraction" stylesheet
  # NOTE: RUNS TABLE EXTRACTION FROM INSIDE EXTRACT
NOTES="../applications/docx-extract/handle-notes.xsl"                            # "Refinement" stylesheets
SCRUB="../applications/docx-extract/scrub.xsl"
JOIN="../applications/docx-extract/join-elements.xsl"
COLLAPSEPARA="../applications/docx-extract/collapse-paragraphs.xsl"

LINKS="../applications/htmlevator/applications/hyperlink-inferencer/hyperlink-inferencer.xsl"
PROMOTELISTS="../applications/list-promote/PROMOTE-lists.xsl"
  # NOTE: RUNS mark-lists, then itemize-lists

# HEADER PROMOTION
HEADERCHOOSEANDPROMOTE="../applications/htmlevator/applications/header-promote/header-promotion-CHOOSE.xsl"

DIGESTPARA="../applications/htmlevator/applications/header-promote/digest-paragraphs.xsl"
MAKEHEADERXSLT="../applications/htmlevator/applications/header-promote/make-header-escalator-xslt.xsl"

FINALRINSE="../applications/html-polish/final-rinse.xsl"

# TYPESCRIPT
UCPTEXT="../applications/htmlevator/applications/ucp-cleanup/ucp-text-macros.xsl"
UCPMAP="../applications/htmlevator/applications/ucp-cleanup/ucp-mappings.xsl"


SPLITONBR="../applications/typescript/p-split-around-br.xsl"
EDITORIABASIC="../applications/typescript/editoria-basic.xsl"
EDITORIAREDUCE="../applications/typescript/editoria-reduce.xsl"


XMLTOHTML5="../applications/html-polish/html5-serialize.xsl"
# INDUCESECTIONS="../applications/htmlevator/applications/induce-sections/induce-sections.xsl"

# Intermediate and final outputs (serializations) are all left on the file system.

$saxonHE -xsl:$EXTRACT -s:$P/$DOCNAME/word/document.xml -o:../outputs/$BOOKNAME/$DOCNAME-1EXTRACTED.xhtml
echo Made $DOCNAME-1EXTRACTED.xhtml

$saxonHE -xsl:$NOTES -s:../outputs/$BOOKNAME/$DOCNAME-1EXTRACTED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-2NOTES.xhtml
echo Made $DOCNAME-2NOTES.xhtml

$saxonHE -xsl:$SCRUB -s:../outputs/$BOOKNAME/$DOCNAME-2NOTES.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-3SCRUBBED.xhtml
echo Made $DOCNAME-3SCRUBBED.xhtml

$saxonHE -xsl:$JOIN -s:../outputs/$BOOKNAME/$DOCNAME-3SCRUBBED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-4JOINED.xhtml
echo Made $DOCNAME-4JOINED.xhtml

$saxonHE -xsl:$COLLAPSEPARA -s:../outputs/$BOOKNAME/$DOCNAME-4JOINED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-5COLLAPSED.xhtml
echo Made $DOCNAME-5COLLAPSED.xhtml

$saxonHE -xsl:$LINKS -s:../outputs/$BOOKNAME/$DOCNAME-5COLLAPSED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-6LINKS.xhtml
echo Made $DOCNAME-6LINKS.xhtml

$saxonHE -xsl:$PROMOTELISTS -s:../outputs/$BOOKNAME/$DOCNAME-6LINKS.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-7PROMOTELISTS.xhtml
echo Made $DOCNAME-7PROMOTELISTS.xhtml

$saxonHE -xsl:$HEADERCHOOSEANDPROMOTE -s:../outputs/$BOOKNAME/$DOCNAME-7PROMOTELISTS.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-8HEADERSPROMOTED.xhtml
echo Made $DOCNAME-8HEADERSPROMOTED.xhtml

# CLASSIC HP
# $saxonHE -xsl:$DIGESTPARA -s:../outputs/$BOOKNAME/$DOCNAME-7PROMOTELISTS.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-8DIGESTEDPARA.xhtml
# echo Made $DOCNAME-8DIGESTEDPARA.xhtml
#
# $saxonHE -xsl:$MAKEHEADERXSLT -s:../outputs/$BOOKNAME/$DOCNAME-8DIGESTEDPARA.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-9BESPOKEHEADERXSLT.xsl
# echo Made $DOCNAME-9BESPOKEHEADERXSLT.xsl

# HEADERXSL="../outputs/$BOOKNAME/$DOCNAME-9BESPOKEHEADERXSLT.xsl"
#
# $saxonHE -xsl:$HEADERXSL -s:../outputs/$BOOKNAME/$DOCNAME-7PROMOTELISTS.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-10CLASSICHEADERSPROMOTED.xhtml
# echo Made $DOCNAME-10CLASSICHEADERSPROMOTED.xhtml


$saxonHE -xsl:$FINALRINSE -s:../outputs/$BOOKNAME/$DOCNAME-8HEADERSPROMOTED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-9RINSED.xhtml
echo Made $DOCNAME-9RINSED.xhtml

$saxonHE -xsl:$UCPTEXT -s:../outputs/$BOOKNAME/$DOCNAME-9RINSED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-10UCPTEXTED.xhtml
echo Made $DOCNAME-10UCPTEXTED.xhtml

$saxonHE -xsl:$UCPMAP -s:../outputs/$BOOKNAME/$DOCNAME-10UCPTEXTED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-11UCPMAPPED.xhtml
echo Made $DOCNAME-11UCPMAPPED.xhtml


$saxonHE -xsl:$SPLITONBR -s:../outputs/$BOOKNAME/$DOCNAME-11UCPMAPPED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-12SPLITONBR.xhtml
echo Made $DOCNAME-12SPLITONBR.xhtml

# $saxonHE -xsl:$EDITORIANOTES -s:../outputs/$BOOKNAME/$DOCNAME-12SPLITONBR.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-13EDITORIANOTES.xhtml
# echo Made $DOCNAME-13EDITORIANOTES.xhtml

$saxonHE -xsl:$EDITORIABASIC -s:../outputs/$BOOKNAME/$DOCNAME-12SPLITONBR.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-13EDITORIABASIC.xhtml
echo Made $DOCNAME-13EDITORIABASIC.xhtml

$saxonHE -xsl:$EDITORIAREDUCE -s:../outputs/$BOOKNAME/$DOCNAME-13EDITORIABASIC.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-14EDITORIAREDUCE.html
echo Made $DOCNAME-14EDITORIAREDUCE.html

$saxonHE -xsl:$XMLTOHTML5 -s:../outputs/$BOOKNAME/$DOCNAME-14EDITORIAREDUCE.html -o:../outputs/$BOOKNAME/$DOCNAME-15HTML5.html
echo Made $DOCNAME-15HTML5.html
